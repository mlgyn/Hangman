package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

func choisirMotAleatoire(fichier string) (string, error) {
	fichierTexte, err := os.Open(fichier)
	if err != nil {
		return "", err
	}
	defer fichierTexte.Close()
	scanner := bufio.NewScanner(fichierTexte)
	var mots []string
	for scanner.Scan() {
		mot := scanner.Text()
		mots = append(mots, mot)
	}
	if len(mots) == 0 {
		return "", fmt.Errorf(Red + "the file is empty" + Reset)
	}
	rand.Seed(time.Now().UnixNano())
	indexAleatoire := rand.Intn(len(mots))
	return mots[indexAleatoire], nil
}

func masquerMot(mot string) string {
	return strings.Repeat(" _ ", len(mot)) // Remplacez l'espace par un espace insécable
}

func Lettre(s string) bool {
	return len(s) == 1 && 'a' <= s[0] && s[0] <= 'z'
}
func mettreAJourMot(motSecret, motMystere, lettre string) string {
	nouveauMot := ""
	lettreTrouvee := false
	for i := 0; i < len(motSecret); i++ {
		if motSecret[i] == lettre[0] {
			nouveauMot += string(lettre[0])
			lettreTrouvee = true
		} else {
			nouveauMot += string(motMystere[i])
		}
	}
	if !lettreTrouvee {
		return motMystere
	}
	return nouveauMot
}
