package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

const (
	Reset  = "\033[0m"
	Red    = "\033[31m"
	Green  = "\033[32m"
	Yellow = "\033[33m"
	Blue   = "\033[34m"
	Purple = "\033[35m"
)

func AffichePendueFromFile(erreurs int) {
	fileName := fmt.Sprintf("hangman_%d.txt", erreurs-1)
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("Erreur lors de l'ouverture du fichier:", err)
		return
	}
	defer file.Close()

	fmt.Println(Purple + " ")

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	fmt.Println(" " + Reset)
}

func main() {
	fmt.Println(" ")
	fmt.Println("Welcome in Hangman !")
	fmt.Println("Your mission, if you accept, save the men from the death.")
	fmt.Println("Press enter to start the mission!")
	var dummy string
	fmt.Scanln(&dummy)
	clear()

	for {
		changeLevel := true
		for changeLevel {
			// Choisissez un niveau avant chaque nouvelle partie
			niveauFichier, err := choisirNiveau()
			if err != nil {
				fmt.Println("Error:", err)
				return
			}

			clear()

			motSecret, err := choisirMotAleatoire(niveauFichier)
			if err != nil {
				fmt.Println("Erreur :", err)
				return
			}

			for {
				motMystere := masquerMot(motSecret)
				tentativesRestantes := 10
				lettresDejaDonnee := ""

				for tentativesRestantes > 0 {
					fmt.Println(" ")
					fmt.Printf(Blue+"\nRemaining attempts : %d\n"+Reset, tentativesRestantes)
					fmt.Printf(Yellow+"Letters already given : [%s]\n"+Reset, lettresDejaDonnee)

					if motMystere == motSecret {
						fmt.Printf(Blue+"\nThe mystery word : %s\n"+Reset, motMystere)
						fmt.Println(Green + "Congratulations! You've guessed a letter !" + Reset)
						fmt.Println(" ")
						break
					}

					// Affiche le pendu après la première erreur
					if tentativesRestantes < 10 {
						AffichePendueFromFile(10 - tentativesRestantes)
					}

					fmt.Printf(Blue+"The mystery word : %s\n"+Reset, motMystere)
					fmt.Print(Blue + "Guess a letter :\n" + Reset)
					fmt.Println(" ")

					var lettre string
					fmt.Scanln(&lettre)
					if len(lettre) != 1 || !Lettre(lettre) {
						clear()
						fmt.Println(Red + "Please enter a single valid letter." + Reset)
						fmt.Println(" ")
						continue
					}
					if strings.Contains(lettresDejaDonnee, lettre) {
						clear()
						fmt.Printf(Yellow+"You've already guessed this letter %s.\n"+Reset, lettre)
						fmt.Println(" ")
						continue
					}
					lettresDejaDonnee += lettre
					if strings.Contains(motSecret, lettre) {
						clear()
						motMystere = mettreAJourMot(motSecret, motMystere, lettre)
						fmt.Printf(Green + "Good guess !\n" + Reset)
					} else {
						clear()
						fmt.Printf(Red+"The letter %s is not in the mystery word.\n"+Reset, lettre)
						fmt.Println(" ")
						tentativesRestantes--
						if tentativesRestantes <= 0 {
							clear()
							fmt.Printf(Red + "You've failed the mission.\n You didn't save the man.")
							fmt.Printf(Red+"The word mystery was : %s\n"+Reset, motSecret)
							AffichePendueFromFile(10)
							break
						}
					}
					if motMystere == motSecret {
						fmt.Printf(Blue+"\nThe mystery word : %s\n"+Reset, motMystere)
						fmt.Println(Green + "Congratulations! You've guessed the mystery word !\n You've save the man !" + Reset)
						fmt.Println(" ")
						break
					}
				}

				replay, newLevel, err := again()
				if err != nil {
					fmt.Println("Error:", err)
					return
				}

				if !replay {
					fmt.Println("Thanks for playing!")
					return
				}

				// Si le joueur veut rejouer et changer de niveau, sortez de la boucle interne
				if newLevel {
					changeLevel = true
					break
				}

				// Sinon, choisissez un nouveau mot pour la prochaine partie
				motSecret, err = choisirMotAleatoire(niveauFichier)
				if err != nil {
					fmt.Println("Erreur :", err)
					return
				}
			}
		}
	}
}
