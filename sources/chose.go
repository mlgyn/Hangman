package main

import (
	"fmt"
)

func choisirNiveau() (string, error) {
	fmt.Printf("\n")
	fmt.Println(Blue + "Choose the difficulty level:" + Reset)
	fmt.Println(Blue + "1. Easy" + Reset)
	fmt.Println(Blue + "2. Intermediate" + Reset)
	fmt.Println(Blue + "3. Difficult" + Reset)
	fmt.Println(Blue + "4. Impossible\n" + Reset)

	var choix int
	fmt.Print(Blue + "Enter the number corresponding to the desired level:\n" + Reset)
	_, err := fmt.Scanln(&choix)
	if err != nil {
		return "", err
	}

	var fichier string
	switch choix {
	case 1:
		fichier = "words_easy.txt"
	case 2:
		fichier = "words_intermediate.txt"
	case 3:
		fichier = "words_difficult.txt"
	case 4:
		fichier = "words_impossible.txt"
	default:
		return "", fmt.Errorf("Invalid choice. Please enter a number between 1 and 4.")
	}

	return fichier, nil
}
