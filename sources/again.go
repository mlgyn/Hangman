package main

import (
	"fmt"
	"strings"
)

func again() (bool, bool, error) {
	var playAgain, changeLevel string
	fmt.Print("Do you want to play again? (yes/no): ")
	_, err := fmt.Scanln(&playAgain)
	if err != nil {
		return false, false, err
	}

	switch strings.ToLower(playAgain) {
	case "yes":
		fmt.Print("Do you want to change the difficulty level? (yes/no): ")
		_, err := fmt.Scanln(&changeLevel)
		if err != nil {
			return false, false, err
		}
		clear()
		return true, strings.ToLower(changeLevel) == "yes", nil
	case "no":
		return false, false, fmt.Errorf("Game over. Thanks for playing!")
	default:
		return false, false, fmt.Errorf("Invalid input. Please enter 'yes' or 'no'.")
	}
}
